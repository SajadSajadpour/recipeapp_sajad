package com.example.aciliit2.sajad_app_recipe;


 import android.content.Intent;
import android.os.AsyncTask;
 import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
 import android.util.Log;
 import android.view.View;
 import android.widget.AdapterView;
 import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

 import java.util.List;

public class MainActivity extends AppCompatActivity {


    private Spinner spinner1;
    private RecyclerView recyclerView;
    private Button add_btn;
    private List<Recipe> alltasks;
    private RecipeAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview_tasks);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        add_btn = findViewById(R.id.add_btn);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, add_newrecipe_activity.class);
                startActivity(intent);
            }
        });




        spinner1 = findViewById(R.id.spinner1);
        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter.createFromResource(this, R.array.types_arrays,
                android.R.layout.simple_spinner_item);
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinner1.setAdapter(staticAdapter);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("testtest", "onitemselected: "+ i);
                Log.e("testtest", "onitemselected2: "+ staticAdapter.getItem(i).toString());

                if (adapter != null && alltasks != null && alltasks.size() > 0){
                    adapter.updateAndFilter(staticAdapter.getItem(i).toString(), alltasks);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getRecipes();
    }

    private void getRecipes() {
        class GetRecipes extends AsyncTask<Void, Void, List<Recipe>> {

            @Override
            protected List<Recipe> doInBackground(Void... voids) {
                List<Recipe> recipeList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return recipeList;
            }

            @Override
            protected void onPostExecute(List<Recipe> tasks) {
                alltasks = tasks;
                Log.e("testtest", "alltasks size: "+ alltasks.size());

                super.onPostExecute(tasks);
                adapter = new RecipeAdapter(MainActivity.this, tasks);
                recyclerView.setAdapter(adapter);
            }
        }

        GetRecipes gt = new GetRecipes();
        gt.execute();
    }
}
