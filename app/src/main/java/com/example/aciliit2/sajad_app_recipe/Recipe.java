package com.example.aciliit2.sajad_app_recipe;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;

import java.io.Serializable;

@Entity
public class Recipe implements Serializable {


    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "recipe")
    private String recipe;

    @ColumnInfo(name = "desc")
    private String desc;

    @ColumnInfo(name = "Recipe_type")
    private String Recipetype;

    @ColumnInfo(name = "finished")
    private boolean finished;

    @ColumnInfo(name = "Recipe_image")//name = "Recipe_image"
    private String image;



    /*
     * Getters and Setters
     * */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public String getRecipetype() {
        return Recipetype;
    }

    public void setRecipetype(String Recipetype) {
        this.Recipetype = Recipetype;
    }


    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
