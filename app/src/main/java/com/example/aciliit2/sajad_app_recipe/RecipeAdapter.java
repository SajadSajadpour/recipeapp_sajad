package com.example.aciliit2.sajad_app_recipe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipesViewHolder> {

    private Context mCtx;
    private List<Recipe> recipeList;
    private ArrayList<Recipe> recipeListforFilter = new ArrayList<>();

    public RecipeAdapter(Context mCtx, List<Recipe> recipeList) {
        this.mCtx = mCtx;
        this.recipeList = recipeList;
    }
    @NonNull
    @Override
    public RecipesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.listadapter_layout, parent, false);

        return new RecipesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipesViewHolder holder, int position) {

        Recipe t = recipeList.get(position);
        holder.textViewing.setText(t.getRecipe());
        holder.textViewDesc.setText(t.getDesc());
        holder.textViewrcipetype.setText(t.getRecipetype());
        if (!t.getImage().isEmpty()) {
            holder.imgV_add.setImageURI(Uri.parse(t.getImage()));
        }

        if (t.isFinished())
            holder.textViewStatus.setText("Updated");
        else
            holder.textViewStatus.setText("Not Updated");
    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    public void updateAndFilter(String type, List<Recipe> alltasks) {
        recipeListforFilter.clear();
        if (type.equalsIgnoreCase("all")){
            recipeList = alltasks;
        }
        else {
            for (Recipe recipe : alltasks) {
                if (recipe.getRecipetype().equalsIgnoreCase(type)) {
                    recipeListforFilter.add(recipe);
                }
            }
            recipeList = recipeListforFilter;
        }
        this.notifyDataSetChanged();
    }

    class RecipesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView textViewStatus, textViewing, textViewDesc, textViewrcipetype;

        ImageView imgV_add;
        public RecipesViewHolder(View itemView) {
            super(itemView);

            textViewStatus = itemView.findViewById(R.id.textViewStatus);
            textViewing = itemView.findViewById(R.id.textViewing);
            textViewDesc = itemView.findViewById(R.id.textViewDesc);
            textViewrcipetype = itemView.findViewById(R.id.textViewrcipetype);
            imgV_add = itemView.findViewById(R.id.imgV_add);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Recipe recipe = recipeList.get(getAdapterPosition());

            Intent intent = new Intent(mCtx, UpdateRecipe_activity.class);
            intent.putExtra("recipe", recipe);

            mCtx.startActivity(intent);
        }
    }
}
